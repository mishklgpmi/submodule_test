#include <SFML/Graphics.hpp>

int main()
{
    sf::RenderWindow window(sf::VideoMode(500, 500), "SFML works!");

    std::vector<std::shared_ptr<sf::CircleShape>> shapes;

    auto shape1 = std::make_shared<sf::CircleShape>(50.f);
    shape1->setOrigin(50,50);
    shape1->setPosition(0,0);
    shape1->setFillColor(sf::Color::Green);
    shapes.push_back(shape1);

    auto shape2 = std::make_shared<sf::CircleShape>(50.f);
    shape2->setOrigin(50,50);
    shape2->setPosition(100,100);
    shape2->setFillColor(sf::Color::Red);
    shapes.push_back(shape2);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        for(const auto& shape : shapes)
            window.draw(*shape);
        window.display();
    }

    return 0;
}